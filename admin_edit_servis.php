<?php  
    include 'template1.php';
    include 'config.php';
?>
<?php
    // kalau tidak ada id di query string
    if( isset($_GET['booking']) ){
        //ambil id dari query string
    $id = $_GET['booking'];


    // buat query untuk ambil data dari database
    $sql = "SELECT service.id_service, service.keluhan, booking.no_booking,booking.no_urut,booking.tgl_booking, booking.jam, pelanggan.username,pelanggan.tipe_motor ,jenis_service.nama_service, jenis_service.tarif, jenis_service.detail from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice where booking.no_booking=$id";
    $query = mysqli_query($db, $sql);
    $data = mysqli_fetch_assoc($query);
}
?>

<section class="content">
        <div class="container-fluid">
            <!-- Horizontal Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-deep-orange ">
                            <h2>
                                Proses Servis Motor
                            </h2>
                        </div>
                        <div class="body">
                        <form method="post" action="proses_detailservice.php">
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="email_address_2">No Antri</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                	<label for="email_address_2">:</label>
                                    <label>
                                    <input type="hidden" name="id_service" value="<?php echo $data['id_service'] ?>">
                                    <?php echo $data['no_urut'] ?></label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="email_address_2">Nama Customer</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                	<label for="email_address_2">:</label>
                                    <label><?php echo $data['username'] ?></label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="email_address_2">Tanggal</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                	<label for="email_address_2">:</label>
                                    <label><?php echo $data['tgl_booking'] ?></label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="email_address_2">Jam</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                	<label for="email_address_2">:</label>
                                    <label><?php echo $data['jam'] ?></label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="email_address_2">Jenis Servis</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                	<label for="email_address_2">:</label>
                                    <label><?php echo $data['nama_service'] ?></label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="email_address_2">Deskripsi</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <label for="email_address_2">:</label>
                                    <label><?php echo $data['detail'] ?></label>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                    <label for="email_address_2">Keluhan</label>
                                </div>
                                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <label for="email_address_2">:</label>
                                    <label><?php echo $data['keluhan'] ?></label>
                                </div>
                            </div>
                            <!-- ============================================== -->
                            <br>
                            <h2>
                                Tambahan Sparepart
                            </h2>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>ID Barang</th>
                                            <th>Nama Saprepart</th>
                                            <th>Harga</th>
                                            <th>Stok</th>
                                            <th style="width: 50px">jumlah</th>
                                            <th style="width: 50px">Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID Barang</th>
                                            <th>Nama Saprepart</th>
                                            <th>Harga</th>
                                            <th>Stok</th>
                                            <th style="width: 50px">jumlah</th>
                                            <th style="width: 50px">Subtotal</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php
                                        $sql = "SELECT * from sparepart";
                                        $query = mysqli_query($db, $sql);

                                        while($spp = mysqli_fetch_array($query)){
                                    ?>
                                        <tr>
                                            <td><?= $spp['id_sparepart'] ?></td>
                                            <td><?= $spp['nama'] ?></td>
                                            <td><?= $spp['harga'] ?>
                                                <input type='hidden' class='form-control' id="harga<?= $spp['id_sparepart'] ?>" value="<?php echo $spp['harga'] ?>">
                                            </td>
                                            <td><?= $spp['jumlah'] ?></td>
                                            <td>
                                            	<!-- <input  type="text" id="jumlah" class="form-control"> -->
                                                <input style="width: 50px" type='number' class='form-control jml' id="jml<?= $spp['id_sparepart'] ?>" name="jml_<?= $spp['id_sparepart'] ?>" data-id="<?= $spp['id_sparepart'] ?>" min = "<?=$spp['jumlah_beli']?>" max="2" value = "<?=$spp['jumlah_beli']?>" onchange="total()" >
                                            </td>
                                            <td>
                                                <input type='text' readonly='readonly' class='form-control'  id="sub_bayar<?= $spp['id_sparepart'] ?>" name="sub_bayar_<?= $spp['id_sparepart'] ?>">
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="row clearfix">
                            <div class="col-sm-5"><label>Input Mekanik :</label>
                            <select class="form-control show-tick" name="kd_mekanik">
                            <?php
                                $sql = "SELECT * FROM mekanik";
                                $query = mysqli_query($db, $sql);

                                while($mkn = mysqli_fetch_array($query)){
                            ?>
                                <option value="<?= $mkn['id_mekanik'] ?>"><?= $mkn['nama_mekanik'] ?></option>
                            <?php } ?>
                            </select></div>
                            <div class="col-sm-3"><label>Harga Service <?php echo $data['nama_service'] ?>:</label><input type="text" class="form-control" readonly="readonly" value = "<?php echo $data['tarif'] ?>" id="tarif_servis" name="totalbelanja"></div>
                            <?php
                              $sql = "select ifnull(sum(sub_bayar), 0) as total from detail_service where id_service = ".$_GET['id_service'];
                              $query = mysqli_query($db, $sql);
                              $total_sparepart = mysqli_fetch_assoc($query);
                            ?>
                            <div class="col-sm-4"><label>Total Sparepart:</label><input type="text" class="form-control" readonly="readonly" id="totalbelanja" name="totalbelanja" value="<?=$data['tarif']?>"> <button type="submit" class="btn btn-primary m-t-15 waves-effect" name="tambahdetailsparepart">Tambah</button></div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <!-- #END# Horizontal Layout -->
        </div>
    </section>
    <!--  -->
    <script type="text/javascript">
        function totalbelanja() {
        // body...
        var a= parseInt(document.getElementById('tarif_servis').value);
        var gtotal = 0;
        // alert(gtotal);
        $(".jml").each(function(){
          var did=$(this).attr('data-id');
          var total =$("#harga"+did).val();
          var jml = $("#jml"+did).val();
          var subbarang = $("#sub_bayar"+did).val();
          var totalakhir = total*jml;
          gtotal = gtotal + totalakhir;
          // alert(a);
        });
        return a+gtotal+<?=$total_sparepart['total']?>;
      }
      function total() {
          $(".jml").each(function(o_index,o_val){
              //menanmbil list  data array
              var did=$(this).attr('data-id');
              //menangkap nilai dari harga
              var total =$("#harga"+did).val();
              var jml = $("#jml"+did).val();
              var subbarang = $("#sub_bayar"+did).val();
              var totalakhir = total*jml;
              document.getElementById("sub_bayar"+did).value = totalakhir;
              document.getElementById("totalbelanja").value = totalbelanja();
              // console.log(totalakhir);
          });
      }
      total();
    </script>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
