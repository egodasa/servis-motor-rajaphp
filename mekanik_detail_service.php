<?php  
    include 'template2.php';
    include 'config.php';
?>
<?php
    // kalau tidak ada id di query string
    if( isset($_GET['no_booking']) ){
        //ambil id dari query string
    $id = $_GET['no_booking'];


    // buat query untuk ambil data dari database
    $sql = "SELECT motor_lain.no_plat_kerabat, motor_lain.jenis_motor, motor_lain.warna, service.id_service, service.keluhan, booking.no_booking,booking.no_urut,booking.tgl_booking, booking.jam, pelanggan.username,pelanggan.tipe_motor ,jenis_service.nama_service, jenis_service.tarif, jenis_service.detail from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice join motor_lain on service.id_motor = motor_lain.id_motor where booking.no_booking=$id";
    $query = mysqli_query($db, $sql);
    $data = mysqli_fetch_assoc($query);
}
?>

<section class="content">
        <div class="container-fluid">
            <!-- Horizontal Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-deep-orange ">
                            <h2>
                                Detail Service
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                              
                              <div class="col-xs-12">
                                <h2>
                                  Detail Service
                                </h2>
                                <div class="row clearfix">
                                  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                      <label for="email_address_2">No Antri</label>
                                  </div>
                                  <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                    <label for="email_address_2">:</label>
                                      <label>
                                      <input type="hidden" name="id_service" value="<?php echo $data['id_service'] ?>">
                                      <?php echo $data['no_urut'] ?></label>
                                  </div>
                                </div>
                                <div class="row clearfix">  
                                  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                      <label for="email_address_2">Nama Customer</label>
                                  </div>
                                  <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <label for="email_address_2">:</label>
                                      <label><?php echo $data['username'] ?></label>
                                  </div>
                                </div>
                                  
                                <div class="row clearfix">  
                                  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                      <label for="email_address_2">Tanggal</label>
                                  </div>
                                  <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <label for="email_address_2">:</label>
                                      <label><?php echo $data['tgl_booking'] ?></label>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                      <label for="email_address_2">Jam</label>
                                  </div>
                                  <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <label for="email_address_2">:</label>
                                      <label><?php echo $data['jam'] ?></label>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                      <label for="email_address_2">Motor</label>
                                  </div>
                                  <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <label for="email_address_2">:</label>
                                      <label><?php echo "No Plat ".$data['no_plat_kerabat'].", Merk ".$data['jenis_motor'].", Warna ".$data['warna'] ?></label>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                      <label for="email_address_2">Jenis Servis</label>
                                  </div>
                                  <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                    <label for="email_address_2">:</label>
                                      <label><?php echo $data['nama_service'] ?></label>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                      <label for="email_address_2">Deskripsi</label>
                                  </div>
                                  <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                      <label for="email_address_2">:</label>
                                      <label><?php echo $data['detail'] ?></label>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                      <label for="email_address_2">Keluhan</label>
                                  </div>
                                  <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                      <label for="email_address_2">:</label>
                                      <label><?php echo $data['keluhan'] ?></label>
                                  </div>
                                </div>
                              </div>
                              <div class="col-xs-12">
                                <h2>Daftar Sparepart</h2>
                                <div class="table-responsive">
                                    <table class="table table-hover dashboard-task-infos">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Jumlah</th>
                                                <th>Sub Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $total = 0;
                                                $sql = "select a.id_service, a.id_detail,a.jumlah, a.sub_bayar, b.nama, b.harga from detail_service a join sparepart b on a.id_sparepart = b.id_sparepart WHERE a.id_service= ".$_GET['id_service'].";";
                                                $query = mysqli_query($db, $sql);
                                                if($query){
                                                  $no = 0;
                                                  while($spp = mysqli_fetch_array($query)){
                                                  $no++;      
                                                  $total+= $spp['sub_bayar'];
                                                  ?>
                                                  <tr>
                                                      <td><?= $no ?></td>
                                                      <td><?= $spp['nama'] ?></td>
                                                      <td><?= $spp['jumlah'] ?></td>
                                                      <td><?= $spp['sub_bayar'] ?></td>
                                                  </tr>
                                            <?php
                                                  }
                                                }
                                            ?>
                                        </tbody>
                                        <tfoot>
                                          <tr>
                                              <th colspan=3 style="text-align:right;">Sub Total</th>
                                              <th style="text-align:left;"><?= $total ?></th>
                                          </tr>
                                          <tr>
                                              <th colspan=3 style="text-align:right;">Tarif Service</th>
                                              <th style="text-align:left;"><?= $data['tarif'] ?></th>
                                          </tr>
                                          <tr>
                                              <th colspan=3 style="text-align:right;">Total Biaya</th>
                                              <th style="text-align:left;"><?= $total+$data['tarif'] ?></th>
                                          </tr>
                                        </tfoot>
                                    </table>
                                </div>
                              </div>
                            </div>
                            <!-- ============================================== -->
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Horizontal Layout -->
        </div>
    </section>
    <!--  -->
    <script>
      <?php 
        $result = mysqli_query($db, "select * from sparepart where id_sparepart not in (select id_sparepart from detail_service where id_service = $_GET[id_service]) and id_sparepart not in (select id_sparepart from pembelian_barang where id_service = $_GET[id_service])");
        $rows = array();
          while($r = mysqli_fetch_assoc($result)) {
            array_push($rows, $r);
          }
      ?>
      var barang = <?php print json_encode($rows);?>
      
      // Fungsi
      function fillSelect(list, caption){
        var dom_select = document.getElementById("barang");
        var l = list.length;
        for(var x = 0; x < l; x++){
          dom_select.innerHTML += "<option value='" + x + "'>" + list[x][caption] + "</option>"
        }
      }
      
      function hitungSubTotal(){
        document.getElementById("sub_total").value = document.getElementById("harga").value * document.getElementById("jumlah").value
      }
      
      // Event 
      document.getElementById('barang').onchange = function(){
        document.getElementById("harga").value = barang[this.value].harga
        document.getElementById("id_sparepart").value = barang[this.value].id_sparepart
      }
      document.getElementById('jumlah').onkeyup = function(){
        hitungSubTotal();
      }
      
      // Program
      fillSelect(barang, "nama");
    </script>


    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
