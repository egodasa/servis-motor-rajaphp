<?php
    include 'config.php';

    if(isset($_GET['bulan']) && isset($_GET['tahun'])){
        $bln = $_GET['bulan'];
        $thn = $_GET['tahun'];
    } 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body onload="window.print()">
<p align="center">TOKO SPAREPART THAICO</p>
<hr />
<center>
  Laporan Transaksi Service <br />
  Bulan: <?= $bln ?> , Tahun : <?= $thn ?>
</center>
<table width="100%" border="1">
  <tr>
    <th width="3%">No</th>
    <th width="50%">Tanggal</th>
    <th width="47%">Total Pemasukan</th>
  </tr>
  <?php 
    $sql = mysqli_query($db,"SELECT booking.no_booking, day(booking.tgl_booking) as tgl_booking, pelanggan.username, pelanggan.no_plat, jenis_service.nama_service,jenis_service.tarif,jenis_service.kode_service,sum(service.total_bayar) as total_bayar from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice WHERE booking.status='1' AND YEAR(booking.tgl_booking)='$_GET[tahun]' AND MONTH(booking.tgl_booking)='$_GET[bulan]' GROUP BY DAY(booking.tgl_booking)");
    $no = 1;
    $total = 0;
   while($data = mysqli_fetch_array($sql)){
    $total += $data['total_bayar'];
  ?>
  <tr>
    <td><?= $no++; ?></td>
    <td><?= $data['tgl_booking'] ?></td>
    <td><?= $data['total_bayar'] ?></td>
  </tr>
  <?php }  ?>
  <tfoot>
      <tr>
          <td colspan="2"><b>Total Pemasukan :</b> </td>
          <td><?= $total ?></td>
      </tr>
  </tfoot>
</table>
<table width="80%" align="center">
  <tr>
    <td colspan="2"></td>
    <td width="286"></td>
  </tr>
  <tr>
    <td width="230" align="center"><br />
    </td>
    <td width="530"></td>
    <td align="center">
    <?php  
      $bulan = array(
                '01' => 'JANUARI',
                '02' => 'FEBRUARI',
                '03' => 'MARET',
                '04' => 'APRIL',
                '05' => 'MEI',
                '06' => 'JUNI',
                '07' => 'JULI',
                '08' => 'AGUSTUS',
                '09' => 'SEPTEMBER',
                '10' => 'OKTOBER',
                '11' => 'NOVEMBER',
                '12' => 'DESEMBER',
        );
    ?>

    Padang, <?php echo date('d').' '.(strtolower($bulan[date('m')])).' '.date('Y') ?></td>
  </tr>
  <tr>
    <td align="center"><br />
        <br />
      <br />
      <br />
      <br />
        <br />
      <br />
      <br /></td>
    <td>&nbsp;</td>
    <td align="center" valign="top"><br />
        <br />
      <br />
      <br />
      <br />
      ( ...................... )<br />
    </td>
  </tr>
</table>
</body>
</html>
