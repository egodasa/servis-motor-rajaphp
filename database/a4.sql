-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id_admin` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id_admin`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `admin` (`id_admin`, `username`, `password`, `nama`) VALUES
(1,	'admin01',	'b59c67bf196a4758191e42f76670ceba',	'admin01'),
(2,	'admin',	'21232f297a57a5a743894a0e4a801fc3',	'admin');

DROP TABLE IF EXISTS `booking`;
CREATE TABLE `booking` (
  `no_booking` int(10) NOT NULL AUTO_INCREMENT,
  `tgl_booking` date NOT NULL,
  `jam` time NOT NULL,
  `no_urut` int(10) NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`no_booking`),
  KEY `id_pelanggan` (`id_pelanggan`),
  CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `booking` (`no_booking`, `tgl_booking`, `jam`, `no_urut`, `id_pelanggan`, `status`) VALUES
(1,	'2018-12-17',	'17:38:17',	1,	9,	1),
(2,	'2018-12-17',	'22:42:40',	2,	9,	1);

DROP TABLE IF EXISTS `detail_service`;
CREATE TABLE `detail_service` (
  `id_detail` int(10) NOT NULL AUTO_INCREMENT,
  `id_service` int(10) NOT NULL,
  `id_sparepart` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `sub_bayar` int(10) NOT NULL,
  PRIMARY KEY (`id_detail`),
  KEY `id_sparepart` (`id_sparepart`),
  KEY `id_service` (`id_service`),
  KEY `id_service_2` (`id_service`),
  CONSTRAINT `detail_service_ibfk_1` FOREIGN KEY (`id_sparepart`) REFERENCES `sparepart` (`id_sparepart`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_service_ibfk_2` FOREIGN KEY (`id_service`) REFERENCES `service` (`id_service`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `detail_service` (`id_detail`, `id_service`, `id_sparepart`, `jumlah`, `sub_bayar`) VALUES
(1,	1,	11,	1,	61000),
(2,	1,	88,	1,	426000),
(3,	2,	1,	1,	4155000),
(4,	2,	2,	1,	2000000);

DROP TABLE IF EXISTS `jenis_service`;
CREATE TABLE `jenis_service` (
  `id_jenisservice` int(10) NOT NULL AUTO_INCREMENT,
  `kode_service` varchar(10) NOT NULL,
  `nama_service` varchar(20) NOT NULL,
  `detail` text NOT NULL,
  `tarif` int(10) NOT NULL,
  PRIMARY KEY (`id_jenisservice`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `jenis_service` (`id_jenisservice`, `kode_service`, `nama_service`, `detail`, `tarif`) VALUES
(1,	'SB',	'Servis Biasa',	'Servis Karbu, Saringan Hawa, Stel Rantai',	25000),
(2,	'CVT',	'Servis CVT',	'Servis Saringan Hawa, Cek Bagian CVT, Bersihan Roiler + Kabel',	40000),
(3,	'BSB',	'Bongkar Setengah',	'Membersihkan Bagian Mesin',	120000),
(4,	'SFM',	'Bongkar Full Mesin',	'Bongkar Mesin Keseluruhan',	200000),
(5,	'SPP',	'Porting Polish',	'Korek Mesin',	100000);

DROP TABLE IF EXISTS `kerabat_anggota`;
CREATE TABLE `kerabat_anggota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_motor` int(11) NOT NULL,
  `nama_kerabat` varchar(100) NOT NULL,
  `no_hp` varchar(13) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `kerabat_anggota` (`id`, `id_motor`, `nama_kerabat`, `no_hp`) VALUES
(2,	3,	'Mimin',	'087878788776'),
(3,	4,	'Ummul',	'089624274286'),
(4,	5,	'Harry',	'089775468966'),
(5,	6,	'Kerabat',	'081234567'),
(6,	7,	'Kaki',	'081234567'),
(7,	7,	'Kaki',	'0812345676'),
(8,	9,	'Kerabat Kamu',	'08122312999'),
(9,	10,	'Nama Kerabat',	'081234533'),
(10,	11,	'Kerabat',	'09882321'),
(11,	12,	'Kerabat 1',	'081234567'),
(12,	13,	'Kerabat 2',	'12345'),
(13,	14,	'afrian',	'089977886655'),
(14,	15,	'',	''),
(15,	15,	'',	'');

DROP TABLE IF EXISTS `mekanik`;
CREATE TABLE `mekanik` (
  `id_mekanik` int(10) NOT NULL AUTO_INCREMENT,
  `nama_mekanik` varchar(50) NOT NULL,
  `nohp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `password` varchar(200) NOT NULL,
  `username` varchar(100) NOT NULL,
  PRIMARY KEY (`id_mekanik`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `mekanik` (`id_mekanik`, `nama_mekanik`, `nohp`, `alamat`, `password`, `username`) VALUES
(2,	'mekanik01',	'081212121212',	'aaaaaaaaaaa',	'd45d5617f01498825df1d36977631617',	'mekanik01'),
(3,	'mekanik02',	'088778767',	'Padang',	'96e79218965eb72c92a549dd5a330112',	'mekanik02');

DROP TABLE IF EXISTS `motor_lain`;
CREATE TABLE `motor_lain` (
  `id_motor` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) NOT NULL,
  `no_plat_kerabat` varchar(10) NOT NULL,
  `jenis_motor` varchar(50) NOT NULL,
  `warna` varchar(30) NOT NULL,
  PRIMARY KEY (`id_motor`),
  KEY `id_pelanggan` (`id_pelanggan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `motor_lain` (`id_motor`, `id_pelanggan`, `no_plat_kerabat`, `jenis_motor`, `warna`) VALUES
(3,	6,	'BA 1212 BB',	'Scoopy',	'Hitam'),
(4,	6,	'BA 1213 BB',	'Air Max',	'Merah'),
(5,	6,	'BA 1768',	'Mio Soul',	'Silver'),
(6,	9,	'BA1234OV',	'Bebek',	'Hitam'),
(7,	9,	'BA1234OV',	'Itiak',	'Hitam'),
(8,	9,	'BA1234OV',	'Itiak',	'Hitam'),
(9,	9,	'BA9999OV',	'Yang mahal',	'Biru'),
(10,	10,	'BA1111OJ',	'Bebek',	'Kuning Putih'),
(11,	10,	'BA0000BB',	'Jenis Motor',	'Biru'),
(12,	9,	'BA1234OJ',	'Yamaha',	'Hitam'),
(13,	9,	'BA3223A',	'Suzuki',	'Hitam'),
(14,	11,	'ba 8877 qw',	'vixion',	'Hitam'),
(15,	9,	'',	'',	''),
(16,	9,	'',	'',	'');

DROP TABLE IF EXISTS `pelanggan`;
CREATE TABLE `pelanggan` (
  `id_pelanggan` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nohp` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `tipe_motor` varchar(20) NOT NULL,
  `no_plat` varchar(10) NOT NULL,
  PRIMARY KEY (`id_pelanggan`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `pelanggan` (`id_pelanggan`, `username`, `password`, `email`, `nohp`, `alamat`, `tipe_motor`, `no_plat`) VALUES
(6,	'monajuwitas',	'b59c67bf196a4758191e42f76670ceba',	'sjuwita.mona@gmail.comm',	'082384822141',	'Batusangkar',	'Vario',	'BA 4563 YY'),
(7,	'bing',	'96e79218965eb72c92a549dd5a330112',	'bing@com',	'086767676767',	'Jakarta',	'Vixion',	'BA1111YY'),
(8,	'feri',	'e10adc3949ba59abbe56e057f20f883e',	'feri@gmail.com',	'082170214495',	'padang',	'mio soul',	'BA 1825 MK'),
(9,	'pelanggan',	'7f78f06d2d1262a0a222ca9834b15d9d',	'pelanggan@mail.com',	'081234567889',	'Alamat',	'Bebek',	'BA5332OV'),
(10,	'pelanggan2',	'db9d905c0d8e7f7f608236efda940cd6',	'pelanggan@pln.com',	'0812312312',	'Lamat',	'Yang Mahal',	'BA9991OV'),
(11,	'aulya',	'93e8b6d2faa1faca5140560dc65d0c40',	'aulya@gmail.com',	'0899778866',	'padang',	'mio',	'ba 8899 qn');

DROP TABLE IF EXISTS `pembelian_barang`;
CREATE TABLE `pembelian_barang` (
  `id_pembelian` int(11) NOT NULL AUTO_INCREMENT,
  `id_service` int(11) NOT NULL,
  `id_sparepart` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `sub_total` int(11) NOT NULL,
  PRIMARY KEY (`id_pembelian`),
  KEY `id_service` (`id_service`),
  KEY `id_sparepart` (`id_sparepart`),
  CONSTRAINT `pembelian_barang_ibfk_1` FOREIGN KEY (`id_service`) REFERENCES `service` (`id_service`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pembelian_barang_ibfk_2` FOREIGN KEY (`id_sparepart`) REFERENCES `sparepart` (`id_sparepart`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `service`;
CREATE TABLE `service` (
  `id_service` int(10) NOT NULL AUTO_INCREMENT,
  `no_booking` int(10) NOT NULL,
  `tgl_service` date NOT NULL,
  `kd_mekanik` int(10) DEFAULT NULL,
  `kd_jenisservice` int(10) NOT NULL,
  `total_bayar` int(10) NOT NULL,
  `keluhan` text,
  `id_motor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_service`),
  KEY `kd_mekanik` (`kd_mekanik`),
  KEY `kd_jenisservice` (`kd_jenisservice`),
  KEY `no_booking` (`no_booking`),
  CONSTRAINT `service_ibfk_1` FOREIGN KEY (`kd_jenisservice`) REFERENCES `jenis_service` (`id_jenisservice`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `service_ibfk_2` FOREIGN KEY (`kd_mekanik`) REFERENCES `mekanik` (`id_mekanik`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `service_ibfk_3` FOREIGN KEY (`no_booking`) REFERENCES `booking` (`no_booking`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `service` (`id_service`, `no_booking`, `tgl_service`, `kd_mekanik`, `kd_jenisservice`, `total_bayar`, `keluhan`, `id_motor`) VALUES
(1,	1,	'2018-12-17',	2,	1,	512000,	'Biasa aja',	6),
(2,	2,	'2018-12-17',	2,	1,	6180000,	'Servis malam',	7);

DROP TABLE IF EXISTS `sparepart`;
CREATE TABLE `sparepart` (
  `id_sparepart` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `harga` int(10) NOT NULL,
  `jumlah` int(10) NOT NULL,
  PRIMARY KEY (`id_sparepart`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sparepart` (`id_sparepart`, `nama`, `harga`, `jumlah`) VALUES
(1,	'ENGINE ASSY',	4155000,	94),
(2,	'A.C. GENERATOR',	2000000,	499),
(3,	'GASKET KIT A',	349000,	498),
(4,	'GASKET KIT B',	597000,	499),
(5,	'ROD SET',	5608000,	500),
(6,	'CAM CHAIN KIT',	42000,	500),
(7,	'FUEL FILTER',	190000,	500),
(8,	'ROD KIT CONNECTING',	220000,	500),
(9,	'CON-ROD KIT',	460000,	500),
(10,	'DRIVE CHAIN KIT',	277000,	500),
(11,	'DAMPER SET WHEEL',	61000,	500),
(12,	'SPOKE SET RR',	11000,	500),
(13,	'SPOKE SET REAR A',	41000,	500),
(14,	'SPOKE SET REAR B',	41000,	500),
(15,	'SPOKE SET REAR C',	41000,	500),
(16,	'SHOE SET, BRAKE',	54500,	500),
(17,	'SEAL SET,PISTON',	70000,	500),
(18,	'PAD SET RR',	43000,	500),
(19,	'INSPECTION KIT',	258200,	500),
(20,	'ADAPT,BIT(3/8X1/4',	60500,	500),
(21,	'TORX T40',	45100,	500),
(22,	'TORX T45',	61500,	500),
(23,	'TORX,BIT 20',	15500,	500),
(24,	'TORX,BIT 40',	18500,	500),
(25,	'O2 SENSOR WRENCH',	80000,	498),
(26,	'FORK BOLT WRENCH 41',	1705000,	500),
(27,	'STOPPER PLATE',	654000,	500),
(28,	'DLC SHORT CONNECTOR',	146000,	500),
(29,	'RADIATOR CAP TESTER',	2767000,	500),
(30,	'OIL FILTER WRENCH',	300000,	500),
(31,	'Busi HONDA Win',	12000,	499),
(32,	'Busi HONDA Grand',	12000,	500),
(33,	'Busi HONDA Astrea Platinum',	47500,	500),
(34,	'Busi HONDA Cs-1',	13000,	500),
(35,	'Busi HONDA C90',	12000,	500),
(36,	'Busi HONDA Scoopy',	13000,	500),
(37,	'Busi HONDA Beat',	13000,	500),
(38,	'Busi HONDA Blade',	13500,	500),
(39,	'Busi HONDA Vario',	22000,	500),
(40,	'Busi HONDA Vario 125 fi',	21000,	500),
(41,	'Busi HONDA CBR 250',	285000,	500),
(42,	'Busi HONDA CB 150R',	20000,	500),
(43,	'Busi HONDA Tiger',	15000,	500),
(44,	'Busi HONDA Revo',	13500,	500),
(45,	'Busi HONDA Megapro',	15000,	500),
(46,	'Busi Yamaha Mio J',	13000,	500),
(47,	'Busi Yamaha Mio GT',	13000,	500),
(48,	'Busi Yamaha Soul GT',	13000,	500),
(49,	'Busi Yamaha Vixion',	12000,	500),
(50,	'Busi Yamaha Jupiter',	12000,	500),
(51,	'Busi Yamaha RX-King',	12500,	500),
(52,	'Busi Yamaha Scorpio',	11500,	500),
(53,	'Busi Yamaha Vega ZR',	11500,	500),
(54,	'Dispad Vario Tekno ,DSK',	12000,	500),
(55,	'Prodorem MIO ,Aspira',	30000,	500),
(56,	'Prodorem Jupiter MX ,Unibear',	35000,	500),
(57,	'Komstir Honda',	55000,	1000),
(58,	'Komstir Yamaha',	40000,	1000),
(59,	'Pentil Ban',	8000,	1000),
(60,	'Fuse Mini (5A)',	1500,	1000),
(61,	'Plug A/C Drain',	1000,	1000),
(62,	'Tutup Jok Scoopy',	140000,	0),
(63,	'Tutup Jok Beat',	200000,	100),
(64,	'Tutup Jok Vario ',	250000,	300),
(65,	'Tutup Jok Mio',	100000,	100),
(66,	'Spion Scoopy',	50000,	1000),
(67,	'Spion Vario',	76000,	200),
(68,	'Spion Mio Soul',	100000,	100),
(69,	'Spion Vixion',	76000,	100),
(70,	'Lampu Sein Beat',	40000,	1000),
(71,	'Lampu Sein Vixion',	70000,	1000),
(72,	'Lampu Sein Mio',	45000,	1000),
(73,	'Lampu Sein Tiger',	45000,	500),
(74,	'Lampu Sein Astra',	30000,	1000),
(75,	'Lampu Sein Scoopy',	40000,	400),
(76,	'Shell Oil Ultra',	160000,	100),
(77,	'Fastron Oil',	140000,	99),
(78,	'Enduro Oil',	38000,	99),
(79,	'Federal Oil',	85000,	50),
(80,	'AHM MPX Oil',	32000,	47),
(81,	'Repsol Oil',	31500,	100),
(82,	'Castrol Oil',	31500,	100),
(83,	'Yamalube Oil',	26000,	100),
(84,	'Top 1 Oil',	25000,	91),
(85,	'Aki Motor GS Premium',	228000,	100),
(86,	'Aki Motor GS Astra',	126000,	98),
(87,	'Aki Motor Battery',	298000,	198),
(88,	'Aki Motor Yuasa',	426000,	392);

-- 2018-12-17 16:02:03
