<?php 
include"template3.php";
include 'config.php';
?>
<section class="content">
        <div class="container-fluid">
        	
            <!-- ======================================================= -->
            <div class="row clearfix">
            	<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header bg-pink">
                            <h2>
                                Riwayat Servis Motor
                            </h2>
                        </div>
                        <div class="body">
                            <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th colspan="9"><label>Cetak Riwayat Servis : &nbsp&nbsp&nbsp</label><a href="member_cetakkartuservis.php?id=<?php echo $_SESSION['id_pelanggan']; ?>" class="btn btn-success btn-circle waves-effect waves-circle waves-float"><i class="material-icons">print</i></a></th>
                                        </tr>
                                        <tr>
                                            <th>#</th>
                                            <th>No Antrian</th>
                                            <th>Tanggal Boking</th>
                                            <th>Jenis Servis</th>
                                            <th>Harga</th>
                                            <th>Total Bayar</th>
                                            <th>Mekanik</th>
                                            <th>Status</th>
                                            <th>Cetak</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $id_pelanggan = $_SESSION['id_pelanggan'];
                                        $sql = "SELECT booking.no_booking, booking.no_urut, booking.tgl_booking,jenis_service.nama_service,jenis_service.tarif,service.total_bayar, booking.status,mekanik.nama_mekanik from booking JOIN service ON booking.no_booking= service.no_booking JOIN mekanik ON service.kd_mekanik = mekanik.id_mekanik JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice WHERE booking.id_pelanggan='$id_pelanggan' ORDER BY booking.tgl_booking";
                                        $query = mysqli_query($db, $sql);

                                        while($spp = mysqli_fetch_array($query)){
                                        ?>
                                            <tr>
                                                <td><?= $no++ ?></td>
                                                <td><?= $spp['no_booking'] ?></td>
                                                <td><?= $spp['tgl_booking'] ?></td>
                                                <td><?= $spp['nama_service'] ?></td>
                                                <td><?= $spp['tarif'] ?></td>
                                                <td><?= $spp['total_bayar'] ?></td>
                                                <td><?= $spp['nama_mekanik'] ?></td>
                                                <td><span class="label label-success">selesai</span></td>
                                                <td>
                                                <a href="member_cetakstruk.php?no_booking=<?= $spp['no_booking'] ?>" class="btn btn-default btn-circle waves-effect waves-circle waves-float"><i class="material-icons">print</i></a></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            <br><br>
                        </div>
                    </div>
                </div>
                <!-- ===================== -->
            </div>
            <!-- =============================================== -->
        </div>
    </section>
