<?php 
include"template2.php";
include"config.php";
?>
<body onload="startTime()">
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Home Mekanik</h2>
        </div>
        
       <div class="row">
            
            <div class="col-lg-6 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-brown hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">equalizer</i>
                    </div>
                    <div class="content">
                        <div class="text">Antrian Booking Service</div>
                        <?php
                            date_default_timezone_set('Asia/Jakarta');
                            $sql = "SELECT count(a.no_booking) as nbk from booking a join service b on a.no_booking = b.no_booking where a.tgl_booking = date(now()) and b.kd_mekanik=".$_SESSION['kd_mekanik'];
                            $query = mysqli_query($db, $sql);
                            if($query){
                              while($booking = mysqli_fetch_array($query)){ ?>
                                <div class="number" id="antri"><?= $booking['nbk']?></div>
                              <?php  }
                            }else{
                              echo '<div class="number" id="antri">0</div>';
                            }
                           ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-black hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">access_alarm</i>
                    </div>
                    <div class="content">
                        <?php
                        date_default_timezone_set('Asia/Jakarta');
                         $a = date('j-F-Y');
                         $b = date('Y-m-d');
                        ?>
                        <input type="hidden" name="tanggal" value="<?= $b ?>">
                        <div class="text"><?= $a ?></div>
                        <div class="number"  id="txt" name="jam"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ======================================================= -->
        <div class="row clearfix">
        	<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                    <tr>
                                        <th>No Antri</th>
                                        <th>Tanggal Boking</th>
                                        <th>Jam</th>
                                        <th>Kode Service</th>
                                        <th>Nama Service</th>
                                        <th>Keluhan</th>
                                        <th>Ket</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $id_pelanggan = $_SESSION['id_pelanggan'];
                                        $sql = "SELECT service.kd_mekanik, service.id_service,booking.no_booking, booking.no_urut, booking.tgl_booking,booking.jam,booking.id_pelanggan, jenis_service.kode_service,jenis_service.nama_service,pelanggan.id_pelanggan, pelanggan.username, booking.status, service.keluhan from booking JOIN pelanggan ON booking.id_pelanggan = pelanggan.id_pelanggan JOIN service ON booking.no_booking= service.no_booking JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice where service.kd_mekanik = ".$_SESSION['kd_mekanik']." AND service.tgl_service = date(now()) ORDER BY booking.no_booking";
                                        $query = mysqli_query($db, $sql);

                                        while($spp = mysqli_fetch_array($query)){
                                    ?>
                                        <tr>
                                            <td><?= $spp['no_urut'] ?></td>
                                            <td><?= $spp['tgl_booking'] ?></td>
                                            <td><?= $spp['jam'] ?></td>
                                            <td><?= $spp['kode_service'] ?></td>
                                            <td><?= $spp['nama_service'] ?></td>
                                            <td><?= $spp['keluhan'] ?></td>
                                            <td>
                                              <span class="label bg-orange">Proses</span>
                                            </td>
                                            <td>
                                              <a href="mekanik_detail_service.php?id_service=<?= $spp['id_service'] ?>&no_booking=<?= $spp['no_booking'] ?>" class="btn btn-info btn-sm">Detail Service</a>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ====================== -->
            
            <!-- ================================= -->
        </div>
        <!-- =============================================== -->
    </div>
</section>

<script>
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>
   <!-- Jquery Core Js -->
<script src="plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="plugins/bootstrap/js/bootstrap.js"></script>

<!-- Custom Js -->
<script src="js/admin.js"></script>
<script src="js/pages/ui/modals.js"></script>

<!-- Demo Js -->
<script src="js/demo.js"></script>
<script src="js/tambahan.js"></script>
</body>
