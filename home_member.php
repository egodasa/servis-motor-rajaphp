<?php 
include"template3.php";
include"config.php";
?>
<body onload="startTime()">
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Home Member</h2>
        </div>
        
       <div class="row">
            
            <div class="col-lg-6 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-brown hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">equalizer</i>
                    </div>
                    <div class="content">
                        <div class="text">Antrian Booking Service</div>
                        <?php
                            date_default_timezone_set('Asia/Jakarta');
                            $b = date('Y-m-d');
                            $sql = "SELECT count(no_booking) as nbk from booking where tgl_booking=date(now()) and status='0'";
                            $query = mysqli_query($db, $sql);
                            while($booking = mysqli_fetch_array($query)){            
                        ?>
                        <div class="number" id="antri"><?= $booking['nbk']?></div>
                        <?php  }?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-black hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">access_alarm</i>
                    </div>
                    <div class="content">
                        <?php
                        date_default_timezone_set('Asia/Jakarta');
                         $a = date('j-F-Y');
                         $b = date('Y-m-d');
                        ?>
                        <input type="hidden" name="tanggal" value="<?= $b ?>">
                        <div class="text"><?= $a ?></div>
                        <div class="number"  id="txt" name="jam"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ======================================================= -->
        <div class="row clearfix">
        	<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-10">
                                <button type="button" class="btn btn-default waves-effect" data-toggle="modal" data-target="#smallModal">Tambah Motor</button>
                            </div>
                            <div class="col-sm-2 button-demo js-modal-buttons">
                                <button type="button" data-color="green" class="btn bg-green waves-effect">Daftar</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-task-infos">
                                <thead>
                                    <tr>
                                        <th>No Antri</th>
                                        <th>Tanggal Boking</th>
                                        <th>Jam</th>
                                        <th>Kode Service</th>
                                        <th>Nama Service</th>
                                        <th>Keluhan</th>
                                        <th>Ket</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $id_pelanggan = $_SESSION['id_pelanggan'];
                                        $sql = "SELECT service.id_service,booking.no_booking, booking.no_urut, booking.tgl_booking,booking.jam,booking.id_pelanggan, jenis_service.kode_service,jenis_service.nama_service,pelanggan.id_pelanggan, pelanggan.username, booking.status, service.keluhan from booking JOIN pelanggan ON booking.id_pelanggan = pelanggan.id_pelanggan JOIN service ON booking.no_booking= service.no_booking JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice where booking.status='0' AND booking.id_pelanggan='$id_pelanggan' AND service.tgl_service=date(now()) ORDER BY booking.no_booking";
                                        $query = mysqli_query($db, $sql);

                                        while($spp = mysqli_fetch_array($query)){
                                    ?>
                                        <tr>
                                            <td><?= $spp['no_urut'] ?></td>
                                            <td><?= $spp['tgl_booking'] ?></td>
                                            <td><?= $spp['jam'] ?></td>
                                            <td><?= $spp['kode_service'] ?></td>
                                            <td><?= $spp['nama_service'] ?></td>
                                            <td><?= $spp['keluhan'] ?></td>
                                            <td>
                                              <span class="label bg-orange">Proses</span>
                                            </td>
                                            <td>
                                              <a href="member_pembelian_barang.php?id_service=<?= $spp['id_service'] ?>&no_booking=<?= $spp['no_booking'] ?>" class="btn btn-info btn-sm">Pembelian Barang</a>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ====================== -->
             <!-- For Material Design Colors -->
            <div class="modal fade" id="mdModal" tabindex="-1" role="dialog" onload="startTime()">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Daftar Servis Motor</h4>
                        </div>
                        <div class="modal-body">
                        <form method="POST" action="proses_member_daftarservice.php">
                            <div class="row">
                                <div class="col-md-4">
                                    <label>ID</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-6">
                                    <label><?php echo $_SESSION['id_pelanggan']; ?></label>
                                    <input type="hidden" name="id_pelanggan" value="<?php echo $_SESSION['id_pelanggan']; ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Username</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-6">
                                    <label name="username"><?php echo $_SESSION['username']; ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>No Urut</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-6">
                                <?php
                                    date_default_timezone_set('Asia/Jakarta');
                                    $b = date('Y-m-d');
                                    $sql = "SELECT count(no_booking) as nbk from booking where tgl_booking='$b'";
                                    $query = mysqli_query($db, $sql);  
                                    while($booking = mysqli_fetch_array($query)){
                                    $booking['nbk'] ++;
                                ?>
                                <label><?= $booking['nbk']?></label>
                                <input type="hidden" name="no_urut" value="<?= $booking['nbk']?>">
                                <?php  }?>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Tanggal Booking</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-6">
                                    <label ><?= $b ?></label>
                                    <input type="hidden" name="tgl_booking" value="<?= $b ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Jam</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-6">
                                    <label><?= date('H:i:s') ?></label>
                                    <input type="hidden" name="jam" value="<?= date('H:i:s') ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Motor</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-6">
                                  <select class="form-control show-tick" name="id_motor" id="id_motor">
                                    <option value="" disabled="disabled">-- Please select --</option>
                                    <?php  
                                        $sql = "SELECT * FROM motor_lain WHERE id_pelanggan =".$_SESSION['id_pelanggan'];
                                        $query = mysqli_query($db, $sql);
                                        while($srv = mysqli_fetch_array($query)){
                                    ?>
                                        <option value="<?= $srv['id_motor'] ?>"><?php echo $srv['no_plat_kerabat']." - ".$srv['jenis_motor']." - ".$srv['warna']; ?></option>
                                    <?php  }?>
                                  </select>
                                </div>
                            </div>
                            <!-- ============================= -->
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Jenis Servis</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-6">
                                    <select class="form-control show-tick" name="jenis_service" id="mySelect" onchange="myFunction()">
                                    <option value="" disabled="disabled">-- Please select --</option>
                                    <?php  
                                        $sql = "SELECT * FROM jenis_service";
                                        $query = mysqli_query($db, $sql);
                                        while($srv = mysqli_fetch_array($query)){
                                    ?>
                                        <option value="<?= $srv['id_jenisservice'] ?>"><?= $srv['nama_service'] ?></option>
                                    <?php  }?>
                                </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Harga</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-6" id="hasil">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>Keluhan</label>
                                </div>
                                <div class="col-md-1">
                                    <label>:</label>
                                </div>
                                <div class="col-md-6" id="keluhan">
									<textarea name="keluhan" class="form-control"></textarea>
                                </div>
                            </div>
                            <br>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="daftarServis" class="btn btn-link waves-effect">Daftar Servis</button>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Batal</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ================== -->
            <!-- Small Size -->
            <div class="modal fade" id="smallModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="smallModalLabel">Data Motor</h4>
                        </div>
                        <form method="POST" action="proses_member_daftarmotorlain.php">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>No Plat</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="hidden" name="id_pelanggan" value="<?php echo $_SESSION['id_pelanggan']; ?>">
                                        <input type="text" name="no_plat" value="" class="form-control">
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Jenis Motor</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="jenis_motor" class="form-control" value="" placeholder="Nama Motor">
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Warna</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="warna" class="form-control" value="" >
                                    </div>
                                </div><hr>
                            </div>
                            <div class="modal-header">
                                <h4 class="modal-title" id="smallModalLabel">Data Pemilik Motor/Kerabat</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Nama Kerabat</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="nama_kerabat" value="" class="form-control">
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>No HP</label>
                                    </div>
                                    <div class="col-md-1">
                                        <label>:</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="no_hp" class="form-control" value="" placeholder="Nama Motor">
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" name="daftarmotorlain" class="btn btn-link waves-effect">Simpan Data</button>
                                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- ================================= -->
        </div>
        <!-- =============================================== -->
    </div>
</section>

<script>
<?php if(isset($_GET['tambahmotor'])): ?>
  $('#smallModal').modal('show');
<?php endif; ?>

function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>
   <!-- Jquery Core Js -->
<script src="plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="plugins/bootstrap/js/bootstrap.js"></script>

<!-- Custom Js -->
<script src="js/admin.js"></script>
<script src="js/pages/ui/modals.js"></script>

<!-- Demo Js -->
<script src="js/demo.js"></script>
<script src="js/tambahan.js"></script>
</body>
