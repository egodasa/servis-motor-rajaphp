<?php 
include"template1.php";
?>
<?php 
include"config.php";
?>
<!DOCTYPE html>

<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>
                                Laporan Harian
                            </h2>
                            
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <form role="form"  method="GET" enctype="multipart/form-data">
                                        <tr>
                                            <th colspan="6"><div class="col-md-2"><h5>Tanggal &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp:</h5></div><div class="col-sm-3"><input type="date" name="tanggal" class="form-control" required="required"></div> <div class="col-sm-3">
                                            <button class="btn bg-green waves-effect" type="submit"><i class="material-icons">search</i></button>

                                            <!-- <button type="button" class="btn bg-green waves-effect" type="submit"><i class="material-icons">search</i></button> -->
                                            <a href="admin_cetaklaporan_harian.php?tanggal=<?php echo $_GET['tanggal']?>" style="margin-left: 10px" class="btn btn-primary waves-effect"><i class="material-icons" >print</i></a></th>
                                        </tr>
                                    </form>
                                        <tr>
                                            <th>No Booking</th>
                                            <th>Tanggal Service</th>
                                            <th>Nama Customer</th>
                                            <th>Jenis Servis</th>
                                            <th>Total Servis</th>
                                            <th>Ket</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    <!--  -->
                                    <?php
                                        $tgl = date('Y-m-d');
                                        if(isset($_GET['tanggal'])){
                                        $tgl = $_GET['tanggal'];
                                            $sql = mysqli_query($db,"SELECT booking.no_booking, booking.tgl_booking, pelanggan.username,jenis_service.nama_service,service.total_bayar from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice WHERE booking.status='1' AND booking.tgl_booking='$tgl'");
                                        }
                                          
                                        else
                                        {
                                            $tgl = date('Y-m-d');
                                            $sql = mysqli_query($db,"SELECT booking.no_booking, booking.tgl_booking, pelanggan.username,jenis_service.nama_service,service.total_bayar from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice WHERE booking.status='1' AND booking.tgl_booking='$tgl'");
                                        }
                                        $sumtotal=0;
                                        
                                        while($lap = mysqli_fetch_array($sql)){
                                            $sumtotal+=$lap['total_bayar'];
                                    ?>
                                        <tr>
                                            <td><?= $lap['no_booking'] ?></td>
                                            <td><?= $lap['tgl_booking'] ?></td>
                                            <td><?= $lap['username'] ?></td>
                                            <td><?= $lap['nama_service'] ?></td>
                                            <td><?= $lap['total_bayar'] ?></td>
                                            <td><a href="admin_cetakstruk.php?no_booking=<?= $lap['no_booking'] ?>" style="margin-left: 10px" class="btn btn-success waves-effect"><i class="material-icons" >print</i></a></td>
                                        </tr>
                                        <?php  }?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="4"><div class="col-md-8"></div><div class="col-md-4"><label style="margin-left: 100px">Total Pemasukan <br>Tanggal: <?= $tgl ?></label> </div>
                                            </th>
                                            <th colspan="2"> <?= $sumtotal ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>

   

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
</body>
