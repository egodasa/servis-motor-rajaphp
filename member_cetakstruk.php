
<?php
    include 'config.php';
    // kalau tidak ada id di query string
    if( isset($_GET['no_booking']) ){
        //ambil id dari query string
    $id = $_GET['no_booking'];


    // buat query untuk ambil data dari database
    $sql = "SELECT service.id_service, booking.no_booking,booking.no_urut,booking.tgl_booking, booking.jam, pelanggan.username,pelanggan.no_plat ,jenis_service.nama_service, jenis_service.tarif, jenis_service.detail, mekanik.nama_mekanik, service.total_bayar from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice JOIN mekanik ON service.kd_mekanik = mekanik.id_mekanik where booking.no_booking=$id";
    $query = mysqli_query($db, $sql);
    $data = mysqli_fetch_assoc($query);
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Basic Form Elements | Bootstrap Based Admin Template - Material Design</title>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="
    text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />

</head>
<body onload="window.print();">
<br>
<div class="container-fluid">
    <!-- Checkbox -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        StrukServis Motor - Bengkel X
                        <small>Jl.xxxxxx, No.Telp:(0752)-00000</a></small>
                    </h2>
                </div>
                <div class="body">
                    <div class="demo-checkbox">
                        <label style="width: 100px">No Booking</label>
                        <label style="width: 5px">: <?php echo $data['no_booking'] ?></label>
                    </div>
                    <div class="demo-checkbox">
                        <label style="width: 100px">Penerima Jasa</label>
                        <label style="width: 5px">: <?php echo $data['username'] ?></label>
                    </div>
                    <div class="demo-checkbox">
                        <label style="width: 100px">Tanggal</label>
                        <label style="width: 5px">: <?php echo $data['tgl_booking'] ?></label>
                    </div>
                    <div class="demo-checkbox">
                        <label style="width: 100px">Jam</label>
                        <label style="width: 5px">: <?php echo $data['jam'] ?></label>
                    </div>
                    <div class="demo-checkbox">
                        <label style="width: 100px">Plat Motor</label>
                        <label style="width: 5px">: <?php echo $data['no_plat'] ?></label>
                    </div>
                    <div class="demo-checkbox">
                        <label style="width: 100px">Jenis Servis</label>
                        <label style="width: 5px">: <?php echo $data['nama_service'] ?></label>
                    </div>
                    <div class="demo-checkbox">
                        <label style="width: 100px">Mekanik</label>
                        <label style="width: 5px">: <?php echo $data['nama_mekanik'] ?></label>
                    </div>

                    <h2 class="card-inside-title">Tambahan Sparepart</h2>
                    <div class="demo-checkbox">
                        <table class="table">
                            <tr>
                                <td>#</td>
                                <td>Kode Sparepart </td>
                                <td>Nama</td>
                                <td>Harga</td>
                                <td>Jumlah</td>
                                <td>Subtotal</td>
                            </tr>
                            <?php
                                $sql = "SELECT service.no_booking, sparepart.id_sparepart, sparepart.nama, sparepart.harga, detail_service.jumlah, detail_service.sub_bayar from service JOIN detail_service ON service.id_service= detail_service.id_service JOIN sparepart ON detail_service.id_sparepart= sparepart.id_sparepart WHERE service.no_booking=$id";
                                $query = mysqli_query($db, $sql);
                                $total=0;
                                $no = 0;
                                while($spp = mysqli_fetch_array($query)){
                                    $total+= $spp['sub_bayar'];
                                    $no++;
                            ?>
                            <tr>
                                <td><?=$no?></td>
                                <td><?= $spp['id_sparepart'] ?></td>
                                <td><?= $spp['nama'] ?></td>
                                <td><?= $spp['harga'] ?></td>
                                <td><?= $spp['jumlah'] ?></td>
                                <td><?= $spp['sub_bayar'] ?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="5">Biaya Sparepart</td>
                                <td><b><?php echo $total ?></b></td>
                            </tr>
                            <tr>
                                <td colspan="5">Biaya Servis</td>
                                <td><b><?php echo $data['tarif'] ?></b></td>
                            </tr>
                            <tr>
                                <td colspan="5"><h5 style="margin-left: 900px">Total Keseluruhan</h5> </td>
                                <td><h3><b><?php echo $data['tarif']+$total ?></b></h3></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table width="80%" align="center">
                  <tr>
                    <td colspan="2"></td>
                    <td width="286"></td>
                  </tr>
                  <tr>
                    <td width="230" align="center"><br />
                    </td>
                    <td width="530"></td>
                    <td align="center"><?php  
                      $bulan = array(
                                '01' => 'JANUARI',
                                '02' => 'FEBRUARI',
                                '03' => 'MARET',
                                '04' => 'APRIL',
                                '05' => 'MEI',
                                '06' => 'JUNI',
                                '07' => 'JULI',
                                '08' => 'AGUSTUS',
                                '09' => 'SEPTEMBER',
                                '10' => 'OKTOBER',
                                '11' => 'NOVEMBER',
                                '12' => 'DESEMBER',
                        );
                    ?>

                    Padang, <?php echo date('d').' '.(strtolower($bulan[date('m')])).' '.date('Y') ?></td>
                  </tr>
                  <tr>
                    <td align="center"><br />
                        <br />
                      <br />
                      <br />
                      <br />
                        <br />
                      <br />
                      <br /></td>
                    <td>&nbsp;</td>
                    <td align="center" valign="top"><br />
                        <br />
                      <br />
                      <br />
                      <br />
                      ( ...................... )<br />
                    </td>
                  </tr>
                </table>
            </div>
        </div>
    </div>
    <!-- #END# Checkbox -->
</div>

</body>
</html>
