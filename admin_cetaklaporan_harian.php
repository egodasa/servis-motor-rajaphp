<?php
include 'config.php';

if(isset($_GET['tanggal'])){
    $tgl = $_GET['tanggal'];
} 
$bulan = array(
            '01' => 'JANUARI',
            '02' => 'FEBRUARI',
            '03' => 'MARET',
            '04' => 'APRIL',
            '05' => 'MEI',
            '06' => 'JUNI',
            '07' => 'JULI',
            '08' => 'AGUSTUS',
            '09' => 'SEPTEMBER',
            '10' => 'OKTOBER',
            '11' => 'NOVEMBER',
            '12' => 'DESEMBER',
    );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body onload="window.print()">
<p align="center">TOKO SPAREPART THAICO</p>
<hr />
<center>
  Laporan Transaksi Service <br />
  Tanggal: <?= $tgl ?>
</center>
<table width="100%" border="1">
  <tr>
    <td width="3%"><div align="center">No</div></td>
    <td width="10%"><div align="center">Kode Service </div></td>
    <td width="14%"><div align="center">Nama</div></td>
    <td width="9%"><div align="center">Status </div></td>
    <td width="11%"><div align="center">No Plat </div></td>
    <td width="13%"><div align="center">Jasa Service </div></td>
    <td width="9%"><div align="center">Biaya Service </div></td>
    <td width="10%"><div align="center">Biaya Sparepart </div></td>
    <td width="6%"><div align="center">Diskon</div></td>
    <td width="15%"><div align="center">Jumlah Bayar </div></td>
  </tr>
  <?php 
    $sql = mysqli_query($db,"SELECT booking.no_booking, booking.tgl_booking, pelanggan.username, pelanggan.no_plat, jenis_service.nama_service,jenis_service.tarif,jenis_service.kode_service,service.total_bayar from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice WHERE booking.status='1' AND booking.tgl_booking LIKE '$_GET[tanggal]'");
    $no = 1;
    $total = 0;
   while($data = mysqli_fetch_array($sql)){
    $total += $data['total_bayar'];
  ?>
  <tr>
    <td><?= $no++; ?></td>
    <td><?= $data['kode_service'] ?></td>
    <td><?= $data['username'] ?></td>
    <td>Selesai</td>
    <td><?= $data['no_plat'] ?></td>
    <td><?= $data['nama_service'] ?></td>
    <td><?= $data['tarif'] ?></td>
    <td><?= $data['total_bayar'] - $data['tarif'] ?></td>
    <td>-</td>
    <td><?= $data['total_bayar'] ?></td>
  </tr>
  <?php }  ?>
  <tfoot>
      <tr>
          <td colspan="9"><b>Total Servis :</b> </td>
          <td><?= $total ?></td>
      </tr>
  </tfoot>
</table>
<table width="80%" align="center">
  <tr>
    <td colspan="2"></td>
    <td width="286"></td>
  </tr>
  <tr>
    <td width="230" align="center"><br />
    </td>
    <td width="530"></td>
    <td align="center"><?php  
      
    ?>

    Padang, <?php echo date('d').' '.(strtolower($bulan[date('m')])).' '.date('Y') ?></td>
  </tr>
  <tr>
    <td align="center"><br />
        <br />
      <br />
      <br />
      <br />
        <br />
      <br />
      <br /></td>
    <td>&nbsp;</td>
    <td align="center" valign="top"><br />
        <br />
      <br />
      <br />
      <br />
      ( ...................... )<br />
    </td>
  </tr>
</table>
</body>
</html>
