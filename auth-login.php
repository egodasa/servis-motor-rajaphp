﻿<?php 
  session_start();

    //jika ditemukan session, maka user akan otomatis dialihkan ke halaman admin
    if (isset($_SESSION['username'])) {
        if ($_SESSION['role'] == "admin") {
            header("Location: home_admin.php");
        }
        elseif ($_SESSION['role'] == "member") {
            header("Location: home_member.php");
        }
        elseif ($_SESSION['role'] == "mekanik") {
            header("Location: home_mekanik.php");
        }
        
    }

 ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>THAICO | Sign In</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">THAICO<b>Sparepart</b></a>
            <small>Jasa Servis Motor dan Ganti Sparepart</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="POST" action="auth-cek-login.php">
                    <div class="msg">Login to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <select class="form-control show-tick" name="role">
                            <option value="" disabled="disabled">-- Login Sebagai --</option>
                            <option value="admin">Admin</option>
                            <option value="member">Member</option>
                            <option value="mekanik">Mekanik</option>
                        </select>
                    </div>
                                

                    <div class="row">
                        <div class="col-xs-6 p-t-5"> <a href="auth-daftar.php">Daftar Member Sekarang</a></div>
                        <div class="col-xs-6">
                            <button class="btn btn-block bg-red waves-effect" type="submit" name="login">Login</button>
                        </div>
                    </div>
                </form>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>
    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/examples/sign-in.js"></script>
</body>

</html>
