<?php 
include"template1.php";
?>
<?php 
include"config.php";
?>
<!DOCTYPE html>

<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>
                                Laporan Bulanan
                            </h2>
                            
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable">
                                    <thead>
                                    <form role="form"  method="GET" enctype="multipart/form-data">
                                        <tr>
                                            <th colspan="5"><div class="col-md-1"><h5>Bulan:</h5></div>
                                            <div class="col-sm-2">
                                                <select name="bulan" class="form-control select2">
                                                    <option value="01">Januari</option>
                                                    <option value="02">Februari</option>
                                                    <option value="03">Maret</option>
                                                    <option value="04">April</option>
                                                    <option value="05">Mei</option>
                                                    <option value="06">Juni</option>
                                                    <option value="07">Juli</option>
                                                    <option value="08">Agustus</option>
                                                    <option value="09">September</option>
                                                    <option value="10">Oktober</option>
                                                    <option value="11">November</option>
                                                    <option value="12">Desember</option>
                                                  </select>
                                            </div> 
                                            <div class="col-sm-2">
                                                <select name="tahun" class="form-control select2">
                                                    <?php
                                                    // echo($a);
                                                    for ($i=2000; $i < 2099; $i++) { 
                                                      echo "<option value='$i'>$i</option> ";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                            <button class="btn bg-green waves-effect" type="submit"><i class="material-icons">search</i></button>

                                            <a href="admin_cetaklaporan_bulanan.php?bulan=<?php echo $_GET['bulan']?>&tahun=<?php echo $_GET['tahun']?>" style="margin-left: 10px" class="btn btn-primary waves-effect"><i class="material-icons" >print</i></a>
                                            </th>
                                        </tr>
                                    </form>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal</th>
                                            <th>Total Pemasukan</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    <!--  -->
                                    <?php
                                        if(isset($_GET['bulan']) & isset($_GET['tahun']))
                                        {
                                            $bln = $_GET['bulan'];
                                            $thn = $_GET['tahun'];
                                            $sql = "SELECT booking.no_booking, day(booking.tgl_booking) as tgl_booking, pelanggan.username, pelanggan.no_plat, jenis_service.nama_service,jenis_service.tarif,jenis_service.kode_service,sum(service.total_bayar) as total_bayar from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice WHERE booking.status='1' AND YEAR(booking.tgl_booking)='$_GET[tahun]' AND MONTH(booking.tgl_booking)='$_GET[bulan]' GROUP BY DAY(booking.tgl_booking)";
                                          }
                                          
                                          else{
                                            $bln = date("M");
                                            $thn = date('Y');
                                            $sql = "SELECT booking.no_booking, day(booking.tgl_booking) as tgl_booking, pelanggan.username,jenis_service.nama_service,sum(service.total_bayar) as total_bayar from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice WHERE booking.status='1' AND YEAR(booking.tgl_booking)=YEAR(NOW()) AND MONTH(booking.tgl_booking)=MONTH(NOW()) GROUP BY DAY(booking.tgl_booking)";
                                        }
                                        $query = mysqli_query($db, $sql);
                                        $no = 1;
                                        $sumtotal=0;

                                        while($lap = mysqli_fetch_array($query)){
                                        $sumtotal+=$lap['total_bayar'];

                                    ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $lap['tgl_booking'] ?></td>
                                            <td><?= $lap['total_bayar'] ?></td>
                                        </tr>
                                        <?php  }?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="2"><div class="col-md-8"></div><div class="col-md-4"><label style="margin-left: 100px">Total Pemasukan 
                                            <br> Bulan : <?= $bln ?>, Tahun : <?= $thn ?> </label> </div>
                                            </th>
                                            <th> <?= $sumtotal ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>

   

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
</body>
