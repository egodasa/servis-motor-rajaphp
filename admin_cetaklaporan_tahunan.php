<?php
include 'config.php';

if(isset($_GET['tahun'])){
$thn = $_GET['tahun'];
}
$bulan = array(
    '1' => 'JANUARI',
    '2' => 'FEBRUARI',
    '3' => 'MARET',
    '4' => 'APRIL',
    '5' => 'MEI',
    '6' => 'JUNI',
    '7' => 'JULI',
    '8' => 'AGUSTUS',
    '9' => 'SEPTEMBER',
    '10' => 'OKTOBER',
    '11' => 'NOVEMBER',
    '12' => 'DESEMBER',
);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body onload="window.print()">
<p align="center">TOKO SPAREPART THAICO</p>
<hr />
<center>
  Laporan Transaksi Service <br />
  Tahun: <?= $thn ?>
</center>
<table width="100%" border="1">
  <tr>
      <th width="3%">No</th>
      <th width="47%">Bulan</th>
      <th width="53%">Total Pemasukan</th>
  </tr>
  <?php 
    $sql = mysqli_query($db,"SELECT booking.no_booking, month(booking.tgl_booking) as tgl_booking, pelanggan.username,jenis_service.nama_service,sum(service.total_bayar) as total_bayar from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice WHERE booking.status='1' AND YEAR(booking.tgl_booking)='$thn' GROUP BY month(booking.tgl_booking) ORDER BY month(booking.tgl_booking) asc");
    $no = 1;
    $total = 0;
   while($data = mysqli_fetch_array($sql)){
    $total += $data['total_bayar'];
  ?>
  <tr>
      <td><?= $no ?></td>
      <td><?= $bulan[$data['tgl_booking']] ?></td>
      <td><?= $data['total_bayar'] ?></td>
  </tr>
  <?php }  ?>
  <tfoot>
      <tr>
          <td colspan="2"><b>Total Pemasukan :</b> </td>
          <td><?= $total ?></td>
      </tr>
  </tfoot>
</table>
<table width="80%" align="center">
  <tr>
    <td colspan="2"></td>
    <td width="286"></td>
  </tr>
  <tr>
    <td width="230" align="center"><br />
    </td>
    <td width="530"></td>
    <td align="center">Padang, <?= date('d-m-Y') ?></td>
  </tr>
  <tr>
    <td align="center"><br />
        <br />
      <br />
      <br />
      <br />
        <br />
      <br />
      <br /></td>
    <td>&nbsp;</td>
    <td align="center" valign="top"><br />
        <br />
      <br />
      <br />
      <br />
      ( ...................... )<br />
    </td>
  </tr>
</table>
</body>
</html>
