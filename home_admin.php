<?php 
include"template1.php";
include 'config.php';
?>
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Halaman Admin</h2>
            </div>
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>Data Servis Hari Ini</h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                        <th>No Booking</th>
                                            <th>No Antri</th>
                                            <th>Jam</th>
                                            <th>Member</th>
                                            <th>Tipe Motor</th>
                                            <th>Jenis Servis</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $sql = "SELECT service.id_service,booking.no_booking,booking.no_urut, booking.jam, pelanggan.username,pelanggan.tipe_motor ,jenis_service.nama_service, booking.status from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice where booking.status='0' AND booking.tgl_booking = date(now()) ORDER BY booking.no_booking";
                                        $query = mysqli_query($db, $sql);

                                        while($spp = mysqli_fetch_array($query)){
                                    ?>
                                        <tr>
                                            <td><?= $spp['no_booking'] ?></td>
                                            <td><?= $spp['no_urut'] ?></td>
                                            <td><?= $spp['jam'] ?></td>
                                            <td><?= $spp['username'] ?></td>
                                            <td><?= $spp['tipe_motor'] ?></td>
                                            <td><?= $spp['nama_service'] ?></td>
                                            <td>
                                            <a href="admin_edit_servis.php?booking=<?= $spp['no_booking'] ?>&id_service=<?= $spp['id_service'] ?>" class="btn bg-teal  waves-effect"><i class="material-icons">edit</i></a>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                
                <!-- Answered Tickets -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="body bg-teal">
                            <div class="font-bold m-b--35">Data Servis</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    Antrian Servis
                                    <?php
                                        date_default_timezone_set('Asia/Jakarta');
                                        $b = date('Y-m-d');
                                        $sql = "SELECT count(no_booking) as nbk from booking where tgl_booking='$b' and status='0'";
                                        $query = mysqli_query($db, $sql);
                                        while($booking = mysqli_fetch_array($query)){            
                                    ?>
                                    <span class="pull-right"><b><?= $booking['nbk']?></b> <small>MOTOR</small></span>
                                    <?php  }?>
                                </li>
                                <li>
                                    Selesai Servis
                                    <?php
                                        date_default_timezone_set('Asia/Jakarta');
                                        $b = date('Y-m-d');
                                        $sql1 = "SELECT count(no_booking) as nbk from booking where tgl_booking='$b' and status='1'";
                                        $query = mysqli_query($db, $sql1);
                                        while($Selesai = mysqli_fetch_array($query)){            
                                    ?>
                                    <span class="pull-right"><b><?= $Selesai['nbk']?></b> <small>MOTOR</small></span>
                                    <?php } ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                    <!-- 
                    
                     -->
                        <div class="body bg-orange">
                            <div class="font-bold m-b--35">Data Servis</div>
                            <ul class="dashboard-stat-list">
                                <li>
                                    Servis Biasa
                                    <span class="pull-right">
                                    <?php
                                        date_default_timezone_set('Asia/Jakarta');
                                        $b = date('Y-m-d');
                                        $sql1 = "SELECT COUNT(id_service) as biasa FROM service JOIN booking ON service.no_booking = booking.no_booking WHERE service.tgl_service='$b' AND service.kd_jenisservice='1' AND booking.status='1'";
                                        $query = mysqli_query($db, $sql1);
                                        while($biasa = mysqli_fetch_array($query)){            
                                    ?>
                                    <b><?= $biasa['biasa']?></b><?php } ?> <small>MOTOR</small></span>
                                </li>
                                <li>
                                    Servis CVT
                                    <span class="pull-right"><?php
                                        date_default_timezone_set('Asia/Jakarta');
                                        $b = date('Y-m-d');
                                        $sql1 = "SELECT COUNT(id_service) as cvt FROM service JOIN booking ON service.no_booking = booking.no_booking WHERE service.tgl_service='$b' AND service.kd_jenisservice='2' AND booking.status='1'";
                                        $query = mysqli_query($db, $sql1);
                                        while($CVT = mysqli_fetch_array($query)){            
                                    ?>
                                    <b><?= $CVT['cvt']?></b><?php } ?> <small>MOTOR</small></span>
                                </li>
                                <li>
                                    Bongkar Setengah
                                    <span class="pull-right"><?php
                                        date_default_timezone_set('Asia/Jakarta');
                                        $b = date('Y-m-d');
                                        $sql1 = "SELECT COUNT(id_service) as bsetengah FROM service JOIN booking ON service.no_booking = booking.no_booking WHERE service.tgl_service='$b' AND service.kd_jenisservice='3' AND booking.status='1'";
                                        $query = mysqli_query($db, $sql1);
                                        while($Bsetengah = mysqli_fetch_array($query)){            
                                    ?>
                                    <b><?= $Bsetengah['bsetengah']?></b><?php } ?> <small>MOTOR</small></span>
                                </li>
                                <li>
                                    Bongkar Full Mesin
                                    <span class="pull-right"><span class="pull-right">
                                    <?php
                                        date_default_timezone_set('Asia/Jakarta');
                                        $b = date('Y-m-d');
                                        $sql1 = "SELECT COUNT(id_service) as bfull FROM service JOIN booking ON service.no_booking = booking.no_booking WHERE service.tgl_service='$b' AND service.kd_jenisservice='4' AND booking.status='1'";
                                        $query = mysqli_query($db, $sql1);
                                        while($Bfull = mysqli_fetch_array($query)){            
                                    ?>
                                    <b><?= $Bfull['bfull']?> </b><?php } ?> <small>MOTOR</small></span>
                                </li>
                                <li>
                                    Porting Polish
                                    <span class="pull-right">
                                    <?php
                                        date_default_timezone_set('Asia/Jakarta');
                                        $b = date('Y-m-d');
                                        $sql1 = "SELECT COUNT(id_service) as pp FROM service JOIN booking ON service.no_booking = booking.no_booking WHERE service.tgl_service='$b' AND service.kd_jenisservice='5' AND booking.status='1'";
                                        $query = mysqli_query($db, $sql1);
                                        while($Pp = mysqli_fetch_array($query)){            
                                    ?>
                                    <b><?= $Pp['pp']?> </b><?php } ?> <small>MOTOR</small></span>
                                </li>
                                
                                <li><b>Total</b>
                                    <span class="pull-right">
                                    <?php
                                        date_default_timezone_set('Asia/Jakarta');
                                        $b = date('Y-m-d');
                                        $sql1 = "SELECT COUNT(id_service) as pp FROM service JOIN booking ON service.no_booking = booking.no_booking WHERE service.tgl_service='$b'  AND booking.status='1'";
                                        $query = mysqli_query($db, $sql1);
                                        while($Pp = mysqli_fetch_array($query)){            
                                    ?>
                                    <b><?= $Pp['pp']?> </b><?php } ?> <small>MOTOR</small></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- #END# Answered Tickets -->
            </div>

            <div class="row clearfix">
                
            </div>
        </div>
    </section>
       

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
