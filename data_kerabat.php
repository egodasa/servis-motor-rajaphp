<?php 
include"template1.php";
?>
<?php 
include"config.php";
?>
<!DOCTYPE html>

<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>
                            Data Kerabat
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                            <th colspan="5"><a href="data_barang_cetak.php" class="btn btn-primary waves-effect"><i class="material-icons">print</i></a></th>
                                        </tr>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Pelanggan</th>
                                            <th>Nama Kerabat</th>
                                            <th>NOHP</th>
                                            <th>No Plat</th>
                                            <th>Jenis Motor</th>
                                            <th>Warna</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $sql = "select b.username,a.*, b.no_plat_kerabat, b.jenis_motor, b.warna from kerabat_anggota a join (select aa.id_motor, aa.no_plat_kerabat, aa.jenis_motor, aa.warna, bb.username from motor_lain aa join pelanggan bb on aa.id_pelanggan = bb.id_pelanggan) b on a.id_motor = b.id_motor;";
                                        $query = mysqli_query($db, $sql);
                                        $no = 0;
                                        while($spp = mysqli_fetch_array($query)){
                                        $no++;
                                    ?>
                                        <tr>
                                            <td><?= $no ?></td>
                                            <td><?= $spp['username'] ?></td>
                                            <td><?= $spp['nama_kerabat'] ?></td>
                                            <td><?= $spp['no_hp'] ?></td>
                                            <td><?= $spp['no_plat_kerabat'] ?></td>
                                            <td><?= $spp['jenis_motor'] ?></td>
                                            <td><?= $spp['warna'] ?></td>
                                        </tr>
                                    <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>

   

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>
    <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Select Plugin Js -->
    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
</body>
