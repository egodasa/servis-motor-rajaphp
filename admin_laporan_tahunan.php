<?php 
include"template1.php";
?>
<?php 
include"config.php";
$bulan = array(
  '1' => 'JANUARI',
  '2' => 'FEBRUARI',
  '3' => 'MARET',
  '4' => 'APRIL',
  '5' => 'MEI',
  '6' => 'JUNI',
  '7' => 'JULI',
  '8' => 'AGUSTUS',
  '9' => 'SEPTEMBER',
  '10' => 'OKTOBER',
  '11' => 'NOVEMBER',
  '12' => 'DESEMBER',
);
?>
<!DOCTYPE html>

<body class="theme-red">
    <section class="content">
        <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>
                                Laporan Tahunan
                            </h2>
                            
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                    <form role="form"  method="GET" enctype="multipart/form-data">
                                        <tr>
                                            <th colspan="5"><div class="col-md-1"><h5>Tahun:</h5></div>
                                            <div class="col-sm-2">
                                                <select name="tahun" class="form-control select2">
                                                    <?php
                                                    // echo($a);
                                                    for ($i=2000; $i < 2099; $i++) { 
                                                      echo "<option value='$i'>$i</option> ";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                            <button class="btn bg-green waves-effect" type="submit"><i class="material-icons">search</i></button>
                                            <a href="admin_cetaklaporan_tahunan.php?tahun=<?php echo $_GET['tahun']?>" style="margin-left: 10px" class="btn btn-primary waves-effect"><i class="material-icons" >print</i></a></div></th>
                                        </tr>
                                    </form>
                                        <tr>
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th>Total Pemasukan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <!--  -->
                                    <?php
                                        if(isset($_GET['tahun'])){
                                            $thn = $_GET['tahun'];
                                            $sql = mysqli_query($db,"SELECT booking.no_booking, month(booking.tgl_booking) as tgl_booking, pelanggan.username,jenis_service.nama_service,sum(service.total_bayar) as total_bayar from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice WHERE booking.status='1' AND YEAR(booking.tgl_booking)='$thn' GROUP BY month(booking.tgl_booking) ORDER BY month(booking.tgl_booking) asc");
                                        }  
                                        else
                                        {
                                            $thn = date("Y");
                                            $sql = mysqli_query($db,"SELECT booking.no_booking, month(booking.tgl_booking) as tgl_booking, pelanggan.username,jenis_service.nama_service,sum(service.total_bayar) as total_bayar from booking JOIN service ON booking.no_booking= service.no_booking JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice WHERE booking.status='1' AND YEAR(booking.tgl_booking)=YEAR(NOW()) GROUP BY month(booking.tgl_booking) ORDER BY month(booking.tgl_booking) asc");
                                        }
                                        $no = 1;
                                        $sumtotal=0;
                                        while($lap = mysqli_fetch_array($sql)){
                                            $sumtotal+=$lap['total_bayar'];
                                    ?>
                                        <tr>
                                            <td><?= $no ?></td>
                                            <td><?= $bulan[$lap['tgl_booking']] ?></td>
                                            <td><?= $lap['total_bayar'] ?></td>
                                        </tr>
                                        <?php  }?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="2"><div class="col-md-8"></div><div class="col-md-4"><label style="margin-left: 100px">Total Pemasukan 
                                            <br> Tahun : <?= $thn ?> </label> </div>
                                            </th>
                                            <th> <?= $sumtotal ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
    </section>

   

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core Js -->
  
    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
</body>
