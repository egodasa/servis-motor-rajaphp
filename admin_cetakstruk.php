<?php
include 'config.php';
$bulan = array(
          '01' => 'JANUARI',
          '02' => 'FEBRUARI',
          '03' => 'MARET',
          '04' => 'APRIL',
          '05' => 'MEI',
          '06' => 'JUNI',
          '07' => 'JULI',
          '08' => 'AGUSTUS',
          '09' => 'SEPTEMBER',
          '10' => 'OKTOBER',
          '11' => 'NOVEMBER',
          '12' => 'DESEMBER',
  );
  // kalau tidak ada id di query string
if( isset($_GET['no_booking']) ){
$id = $_GET['no_booking'];

// buat query untuk ambil data dari database
$sql = "SELECT booking.no_booking, 
						service.id_service,
						booking.tgl_booking, booking.no_urut,
						pelanggan.username, pelanggan.no_plat,
						jenis_service.kode_service, jenis_service.tarif,
						jenis_service.nama_service,
						service.total_bayar,
						mekanik.nama_mekanik
		from booking JOIN service ON booking.no_booking= service.no_booking 
					JOIN mekanik ON service.kd_mekanik= mekanik.id_mekanik
					JOIN pelanggan ON booking.id_pelanggan= pelanggan.id_pelanggan 
					JOIN jenis_service ON service.kd_jenisservice= jenis_service.id_jenisservice 
		WHERE booking.no_booking='$id'";

$query = mysqli_query($db, $sql);
$cs = mysqli_fetch_assoc($query);
$waktu_bulan = explode("-", $cs['tgl_booking']);
}
?>
<body onload="window.print()">
<p align="center">TOKO SPAREPART THAICO</p>
<hr>
<center>FAKTUR SERIVCE<br>

Bulan: <?=$bulan[$waktu_bulan[1]]?></center>
<table width="80%" border="0" align="center">
  <tr>
    <td width="19%">Kode Service </td>
    <td width="41%">: <?= $cs['kode_service'] ?></td>
    <td width="19%">No Booking </td>
    <td width="21%">: <?= $cs['no_booking'] ?></td>
  </tr>
  <tr>
    <td>Tanggal Service </td>
    <td>: <?= $cs['tgl_booking'] ?></td>
    <td>Tanggal Booking </td>
    <td>: <?= $cs['tgl_booking'] ?></td>
  </tr>
  <tr>
    <td>Nama Pelanggan </td>
    <td>: <?= $cs['username'] ?></td>
    <td>No Urut </td>
    <td>: <?= $cs['no_urut'] ?></td>
  </tr>
  <tr>
    <td>No Plat </td>
    <td>: <?= $cs['no_plat'] ?></td>
    <td>Status Pelanggan </td>
    <td>: Member</td>
  </tr>
  <tr>
    <td>Nama Mekanik </td>
    <td>: <?= $cs['nama_mekanik'] ?></td>
    <td>Jasa Service </td>
    <td>: <?= $cs['nama_service'] ?></td>
  </tr>
</table>
<table width="80%" border="1" align="center">
  <tr>
    <td width="5%"><div align="center">No</div></td>
    <td width="16%"><div align="center">Kode Sparepart </div></td>
    <td width="26%"><div align="center">Nama Sparepart </div></td>
    <td width="20%"><div align="center">Harga</div></td>
    <td width="19%"><div align="center">Jumlah</div></td>
    <td width="14%"><div align="center">Sub Bayar </div></td>
  </tr>
  <?php
        $sql = "SELECT service.no_booking, sparepart.id_sparepart, sparepart.nama, sparepart.harga, detail_service.jumlah, detail_service.sub_bayar from service JOIN detail_service ON service.id_service= detail_service.id_service JOIN sparepart ON detail_service.id_sparepart= sparepart.id_sparepart WHERE service.no_booking=$id";
        $query = mysqli_query($db, $sql);
        $b =1;
        $a=0;

        while($spp = mysqli_fetch_array($query)){
            $a += $spp['sub_bayar'];
    ?>
  <tr>
    <td><?= $b++; ?></td>
    <td><?= $spp['id_sparepart'] ?></td>
    <td><?= $spp['nama'] ?></td>
    <td><?= $spp['harga'] ?></td>
    <td><?= $spp['jumlah'] ?></td>
    <td><?= $spp['sub_bayar'] ?></td>
  </tr>
  <?php } ?>
  <tr>
    <td colspan="5"><div align="right">Total Pembayaran </div></td>
    <td><?= $a ?></td>
  </tr>
  <tr>
    <td colspan="5"><div align="right">Biaya Jasa Service </div></td>
    <td><?= $cs['tarif'] ?></td>
  </tr>
  <tr>
    <td colspan="5"><div align="right">Diskon</div></td>
    <td>0</td>
  </tr>
  <tr>
    <td colspan="5"><div align="right">Jumlah Bayar </div></td>
    <td><?= $cs['tarif']+$a ?></td>
  </tr>
</table>
<table width=80% align="center">
  <tr>
    <td colspan="2"></td>
    <td width="286"></td>
  </tr>
  <tr>
    <td width="230" align="center"> <br> 
   </td>
    <td width="530"></td>
	
    <td align="center">

    Padang, <?php echo date('d').' '.(strtolower($bulan[date('m')])).' '.date('Y') ?></td>
  </tr>
  <tr>
    <td align="center"><br /><br /><br /><br /><br />
     <br /><br /><br /></td>
    <td>&nbsp;</td>
    <td align="center" valign="top"><br /><br /><br /><br /><br />
      ( ...................... )<br />
    </td>
  </tr>
  
  
</table>
</body>
