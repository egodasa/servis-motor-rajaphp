<?php 
include"template3.php";
include 'config.php';
?>
<?php  
?>


<section class="content">
        <div class="container-fluid">
            <!-- ======================================================= -->
            <div class="row clearfix">
            	<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h1>
                                Profil Member
                            </h1>
                        </div>
                        <div class="body">
                            <div class="list-group">
                                <a href="javascript:void(0);" class="list-group-item">
                                    <span class="badge bg-purple"><?php echo $_SESSION['id_pelanggan']; ?></span><b>ID Member</b> 
                                </a>
                                <a href="javascript:void(0);" class="list-group-item">
                                    <label class="pull-right"><?php echo $_SESSION['username']; ?></label>Username
                                </a>
                                <a href="javascript:void(0);" class="list-group-item">
                                    <label class="pull-right"><?php echo $_SESSION['email']; ?></label>Email
                                </a>
                                <a href="javascript:void(0);" class="list-group-item">
                                    <label class="pull-right"><?php echo $_SESSION['nohp']; ?></label>No Handphone
                                </a>
                                <a href="javascript:void(0);" class="list-group-item">
                                    <label class="pull-right"><?php echo $_SESSION['alamat']; ?></label>Alamat 
                                </a>
                                <a href="javascript:void(0);" class="list-group-item">
                                    <label class="pull-right"><?php echo $_SESSION['tipe_motor']; ?></label>Tipe Motor

                                </a>
                                <a href="javascript:void(0);" class="list-group-item">
                                    <label class="pull-right"><?php echo $_SESSION['no_plat']; ?></label>No Plat
                                </a>
                            </div>
                            <br><br>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <div class="card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-sm-10">
                                            <h2>Daftar Motor Lain</h2>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-hover dashboard-task-infos">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Kerabat</th>
                                                    <th>No HP</th>
                                                    <th>Plat Motor</th>
                                                    <th>Jenis Motor</th>
                                                    <th>Warna</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $no = 1;
                                                    $id_pelanggan = $_SESSION['id_pelanggan'];
                                                    $sql = "SELECT * FROM motor_lain JOIN pelanggan USING (id_pelanggan) JOIN kerabat_anggota USING (id_motor) WHERE id_pelanggan= '$id_pelanggan'";
                                                    $query = mysqli_query($db, $sql);

                                                    while($spp = mysqli_fetch_array($query)){
                                                ?>
                                                    <tr>
                                                        <td><?= $no++ ?></td>
                                                        <td><?= $spp['nama_kerabat'] ?></td>
                                                        <td><?= $spp['no_hp'] ?></td>
                                                        <td><?= $spp['no_plat_kerabat'] ?></td>
                                                        <td><?= $spp['jenis_motor'] ?></td>
                                                        <td><?= $spp['warna'] ?></td>
                                                        </td>
                                                    </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- ===================== -->
            </div>
            <!-- =============================================== -->
        </div>
    </section>
